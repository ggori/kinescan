﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace KineScan {
    /*
     * Class that handles the on-screen point cloud movement with mouse and keyboard
     * 
     * Currently the follow commands are implemented:
     * W moves the camera forward
     * A moves the camera left
     * S moves the camera backward
     * D moves the camera right
     * SPACE moves the camera up
     * LEFT-CTRL moves the camera down
     * R resets the camera to the origin
     * Pressing the left mouse button and moving the mouse changes the viewing direction
     * Pressing the right mouse button and moving the mouse changes the camera position (left, right, up and down).
     * Using the mouse scroll wheel moves the camera forward/backward
     * 
     */
    class ViewHandler {

        private const int MOUSE_SLOW_FACTOR = 300;
        private const int KEYBOARD_SLOW_FACTOR = 150;
        private const int SCROLL_WHEEL_FACTOR = 600;
        private const int ROTATION_FACTOR = -1;

        private Vector3 camera;
        private Vector3 viewingDirection;
        private Vector3 up;

        private MouseState previousMouse;

        public ViewHandler() {
            previousMouse = Mouse.GetState();
            resetPosition();
        }

        // Updates the current camera position and viewing direction depending on mouse and keyboard state.
        // Returns the new viewport matrix
        public Matrix updateViewport() {

            MouseState currentMouse = Mouse.GetState();
            KeyboardState currentKeyboard = Keyboard.GetState();

            if (currentKeyboard.IsKeyDown(Keys.R)) resetPosition();

            Vector3 right;

            if (previousMouse.RightButton == ButtonState.Pressed && currentMouse.RightButton == ButtonState.Pressed) {
                right = Vector3.Cross(viewingDirection, up);
                camera += right * (previousMouse.X - currentMouse.X) / MOUSE_SLOW_FACTOR;
                camera -= up * (previousMouse.Y - currentMouse.Y) / MOUSE_SLOW_FACTOR;
            }

            right = Vector3.Cross(viewingDirection, up);
            if (currentKeyboard.IsKeyDown(Keys.A)) camera -= right / KEYBOARD_SLOW_FACTOR;
            if (currentKeyboard.IsKeyDown(Keys.D)) camera += right / KEYBOARD_SLOW_FACTOR;
            if (currentKeyboard.IsKeyDown(Keys.W)) camera += viewingDirection / KEYBOARD_SLOW_FACTOR;
            if (currentKeyboard.IsKeyDown(Keys.S)) camera -= viewingDirection / KEYBOARD_SLOW_FACTOR;
            if (currentKeyboard.IsKeyDown(Keys.Space)) camera += up / KEYBOARD_SLOW_FACTOR;
            if (currentKeyboard.IsKeyDown(Keys.LeftControl)) camera -= up / KEYBOARD_SLOW_FACTOR;

            if (previousMouse.LeftButton == ButtonState.Pressed && currentMouse.LeftButton == ButtonState.Pressed) {
                // Horizontal Rotation
                Quaternion rotationHorizontal = Quaternion.CreateFromAxisAngle(up, (float)(currentMouse.X - previousMouse.X) / MOUSE_SLOW_FACTOR * ROTATION_FACTOR);
                viewingDirection = Vector3.Transform(viewingDirection, rotationHorizontal);

                // Vertical Rotation
                right = Vector3.Cross(viewingDirection, up);
                Quaternion rotationVertical = Quaternion.CreateFromAxisAngle(right, (float)(currentMouse.Y - previousMouse.Y) / MOUSE_SLOW_FACTOR * ROTATION_FACTOR);
                viewingDirection = Vector3.Transform(viewingDirection, rotationVertical);
                //up = Vector3.Transform(up, rotationVertical);
            }

            if (currentMouse.MiddleButton == ButtonState.Pressed) {
                right = Vector3.Cross(viewingDirection, up);
                //up = Vector3.Up;
                viewingDirection = -Vector3.Cross(right, up);
            }

            camera += viewingDirection * (currentMouse.ScrollWheelValue - previousMouse.ScrollWheelValue) / SCROLL_WHEEL_FACTOR;

            previousMouse = currentMouse;

            return Matrix.CreateLookAt(camera, camera + viewingDirection, up);
        }

        // Move the camera by the vector v
        public void moveCamera(Vector3 v) {
            camera += v;
        }

        // Resets the camera position to the origin, looking to the direction (0, 0, 1)
        private void resetPosition() {
            camera = Vector3.Zero;
            viewingDirection = Vector3.UnitZ;
            up = Vector3.Up;
        }
    }
}
