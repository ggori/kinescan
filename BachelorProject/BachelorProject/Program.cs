using System;

namespace KineScan {
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Scanner scanner = new Scanner())
            {
                scanner.Run();
            }
        }
    }
#endif
}

