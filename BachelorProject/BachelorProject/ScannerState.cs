﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {
    /**
     * This class represents a state of the scanner. States work in a pipeline, and each state will return the next one when appropriate with the nextState() method.
     */
    abstract class ScannerState {

        /**
         * Variables that are needed in all states, the kinect sensor, the graphics device and the sprite batch
         */
        protected KinectSensor kinect;
        protected GraphicsDevice graphicsDevice;
        protected SpriteBatch spriteBatch;
        protected SpriteFont font;

        /**
         * Constructor
         */
        protected ScannerState(KinectSensor kinect, GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, SpriteFont font) {
            this.kinect = kinect;
            this.graphicsDevice = graphicsDevice;
            this.spriteBatch = spriteBatch;
            this.font = font;
        }

        /**
         * Update the current state. Do whatever is needed
         */
        abstract public void update(GameTime gameTime);

        /**
         * get the next state. Each state may return itself if it is not done, or a new state in the pipeline
         */
        abstract public ScannerState nextState();

        /**
         * Draw the current state
         */
        abstract public void draw(GameTime gameTime);
    }
}
