﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NKH.MindSqualls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {
    class GeneratePointCloudState : ScannerState {

        private Vector3 axis;               // rotation axis
        private Point objectLocation;       // object position in pixel coordinates
        private CoordinateMapper mapper;    // coordinate mapper
        private PointCloud pointCloud;      // the current point cloud
        private List<PointCloud> clouds;    // list of point clouds
        private BasicEffect effect;         // effect, needed for 3d visualization
        private ViewHandler viewHandler;    // viewhandler, needed for moving the 3d visualization
        private NxtMotor motor;             // the LEGO nxt motor
        private double motionStartedTS;     // timestamp of when the motor has started, in seconds
        private double lastFrameTS;         // timestamp of when the last point cloud was captured
        private bool done;                  // True if we are done with this state and we wish to go on with life

        public GeneratePointCloudState(KinectSensor kinect, 
            GraphicsDevice graphicsDevice, 
            SpriteBatch spriteBatch, 
            SpriteFont font,
            Vector3 axis, 
            Point objectLocation)
        : base(kinect, graphicsDevice, spriteBatch, font) {

            this.motionStartedTS = 0;
            this.lastFrameTS = -10;
            this.axis = axis;
            this.objectLocation = objectLocation;
            this.mapper = kinect.CoordinateMapper;
            this.viewHandler = new ViewHandler();
            this.viewHandler.moveCamera(new Vector3(0f, 0f, -0.2f)); // move the camera back a bit
            this.clouds = new List<PointCloud>();
            this.pointCloud = PointCloudFactory.emptyPointCloud();
            this.done = false;

            // Lego base
            NxtBrick brick = new NxtBrick(NxtCommLinkType.USB, 0);
            this.motor = new NxtMotor();
            brick.MotorA = motor;
            motor.Run(50, 0);

            // Graphics
            effect = new BasicEffect(graphicsDevice);
            effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(45),
                (float)graphicsDevice.Viewport.Width / (float)graphicsDevice.Viewport.Height,
                0.1f, 10f); ;
            effect.View = Matrix.Identity;
            effect.LightingEnabled = false;
            effect.VertexColorEnabled = true;

            Console.WriteLine("Now in GeneratePointCloudState - Please wait while the point cloud is being generated");
        }

        public override void update(GameTime gameTime) {
            if (motionStartedTS == 0) {
                motionStartedTS = gameTime.TotalGameTime.TotalSeconds;
            }

            using (DepthImageFrame depthFrame = kinect.DepthStream.OpenNextFrame(0)) {
                using (ColorImageFrame colorFrame = kinect.ColorStream.OpenNextFrame(0)) {
                    if (depthFrame != null && colorFrame != null) {
                        // Get raw depth data
                        DepthImagePixel[] depthData = depthFrame.GetRawPixelData();

                        // Convert the raw depth data into world space
                        SkeletonPoint[] skeletonPoints = new SkeletonPoint[depthFrame.Width * depthFrame.Height];
                        mapper.MapDepthFrameToSkeletonFrame(Scanner.DEPTH_FORMAT, depthData, skeletonPoints);
                    
                        // Create the cloud
                        pointCloud = PointCloudFactory.reducedPointCloud(depthFrame.Width, depthFrame.Height, skeletonPoints, colorFrame, mapper, objectLocation);

                        // elapsed time in seconds
                        double elapsedTime = gameTime.TotalGameTime.TotalSeconds - motionStartedTS;

                        // Move the Cloud
                        pointCloud.translate(-axis);

                        // Rotate the cloud
                        // in order to find the speed, compute the time it takes to do a full rotation, then do 2*pi/time
                        double speed = 0.154663; // rotation speed in radians per second
                        pointCloud.rotate((float)(elapsedTime * speed));

                        // Save the cloud
                        if (elapsedTime - lastFrameTS > 1) {
                            clouds.Add(pointCloud);
                            lastFrameTS = elapsedTime;
                        }

                        // Eventually stop
                        // A full rotation is 2*PI/speed... i think.
                        if (elapsedTime > 30) {
                            done = true;
                            motor.Run(50, 1);
                        }
                    }
                }
            }
            effect.View = viewHandler.updateViewport();
        }

        public override ScannerState nextState() {
            if (!done) {
                return this;
            } else {
                return new MergeCloudsState(kinect, graphicsDevice, spriteBatch, font, clouds, effect, viewHandler);
            }
        }

        public override void draw(GameTime gameTime) {
            graphicsDevice.Clear(Color.White);

            VertexPositionColor[] line = new VertexPositionColor[2];
            for (int k = 0; k < line.Length; ++k) {
                line[k].Color = Color.Red;
            }
            // Rotation axis
            line[0].Position = new Vector3(0, -1, 0);
            line[1].Position = new Vector3(0, 1, 0);
            
            foreach (EffectPass effectPass in effect.CurrentTechnique.Passes) {
                effectPass.Apply();

                if (pointCloud.Length > 0) {
                    graphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, pointCloud.getDrawableList(), 0, pointCloud.Length, VertexPositionColor.VertexDeclaration);
                }

                graphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, line, 0, line.Length / 2, VertexPositionColor.VertexDeclaration);
            }
        }
    }
}
