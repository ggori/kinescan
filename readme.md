# KineScan

B.Sc. Thesis Project at Universita' della Svizzera Italiana under the supervision of Kai Hormann.

Built a 3d scanner using the Kinect sensor and a Lego platform. Experience in Kinect API, point clouds and surface reconstruction. Used C#, OpenGL, Kinect SDK, Lego Mindstorms, Alglib.