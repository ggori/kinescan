﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {
    using Microsoft.Kinect;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    namespace BachelorProject {
        /**
         * The state of the scanner where we visualize the plain color image, and wait for a click in the object
         */
        class FindObjectState : ScannerState {

            private Texture2D colorVideo; // The color image bitmap
            private Vector3 axis;         // Point in 3D (world coordinates)
            private Point objectLocation; // Point in 2D (screen coordinates)
            private bool done;
            private bool clicked;

            public FindObjectState(KinectSensor kinect, GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, SpriteFont font, Vector3 axis)
                : base(kinect, graphicsDevice, spriteBatch, font) {

                this.colorVideo = new Texture2D(graphicsDevice, kinect.DepthStream.FrameWidth, kinect.DepthStream.FrameHeight);
                this.axis = axis;
                this.done = false;
                this.clicked = false;

                Console.WriteLine("Now in FindObjectState - Please click on the object");
            }

            public override void update(GameTime gameTime) {
                // Get the color frame, convert from BGR into RGB, and save it into the texture
                using (ColorImageFrame colorFrame = kinect.ColorStream.OpenNextFrame(0)) {
                    if (colorFrame != null) {
                        byte[] colorData = colorFrame.GetRawPixelData();
                        for (int i = 0; i < colorData.Length; i += colorFrame.BytesPerPixel) {
                            byte tmp = colorData[i];
                            colorData[i] = colorData[i + 2];
                            colorData[i + 2] = tmp;
                        }
                        colorVideo = new Texture2D(graphicsDevice, colorFrame.Width, colorFrame.Height);
                        colorVideo.SetData(colorData);

                        // Check if the mouse has been pressed
                        MouseState currentMouse = Mouse.GetState();
                        if (!clicked
                            && currentMouse.LeftButton == ButtonState.Pressed
                            && currentMouse.X > 0
                            && currentMouse.Y > 0
                            && currentMouse.X < colorFrame.Width
                            && currentMouse.Y < colorFrame.Height) {
                            using (DepthImageFrame depthFrame = kinect.DepthStream.OpenNextFrame(0)) {
                                DepthImagePixel[] depthData = depthFrame.GetRawPixelData();
                                DepthImagePoint[] depthPoints = new DepthImagePoint[colorFrame.Width * colorFrame.Height];
                                kinect.CoordinateMapper.MapColorFrameToDepthFrame(Scanner.COLOR_FORMAT, Scanner.DEPTH_FORMAT, depthData, depthPoints);
                                DepthImagePoint objectLocationPoint = depthPoints[currentMouse.Y * colorFrame.Width + currentMouse.X];
                                objectLocation = new Point(objectLocationPoint.X, objectLocationPoint.Y);
                                if (objectLocation.X > 0 && objectLocation.Y > 0 && objectLocation.X < colorFrame.Width && objectLocation.Y < colorFrame.Height) {
                                    clicked = true;
                                }
                            }
                        }

                        // Check if mouse has then been released
                        if (clicked && currentMouse.LeftButton == ButtonState.Released) {
                            done = true;
                        }
                    }
                }
            }

            public override ScannerState nextState() {
                if (!done) {
                    return this;
                } else {
                    return new GeneratePointCloudState(kinect, graphicsDevice, spriteBatch, font, axis, objectLocation);
                }
            }

            public override void draw(GameTime gameTime) {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
                spriteBatch.Draw(colorVideo, new Rectangle(0, 0, colorVideo.Width, colorVideo.Height), Color.White);
                spriteBatch.DrawString(font, "Please click on the object", Vector2.One, Color.Black);
                spriteBatch.DrawString(font, "Please click on the object", Vector2.Zero, Color.White);
                spriteBatch.End();
            }
        }
    }

}
