\documentclass[hidelinks]{usiinfbachelorproject}

\captionsetup{labelfont={bf}}

\newcommand\fnurl[2]{
#1%\href{#2}{#1}
\footnote{\url{#2}}
}

\author{Giorgio Gori}

\title{\includegraphics[width=\textwidth, trim=0mm 7cm 0mm 3cm]{logo.pdf}}
\versiondate{\today}

\begin{committee}
\advisor[Universit\`a della Svizzera Italiana, Switzerland]{Prof.}{Kai}{Hormann}
\assistant[Universit\`a della Svizzera Italiana, Switzerland]{}{Teseo}{Schneider}
\end{committee}

\abstract {
Photocopying has been popular for years, and we can now translate the problem, which consists of scanning and printing, into the 3D world. 3D printers are now cheaper and more popular, but 3D scanners are still not as widespread.
The goal of this project is to build create a cheap 3D scanner using the Microsoft Kinect capabilities and a bit of \textsc{Lego}.
There are plenty of applications, such as ``copying'' real objects or digitalizing models for games, architecture, simulations, or other 3D applications.
}


\begin{document}
\maketitle

\newpage
\tableofcontents

%\listoffigures

\setlength{\parskip}{1em}

\newpage
\section{Introduction}
\subsection{Motivation}
In the past years 3D printers have become cheaper and more popular, and it is relatively easy to transform a 3D model into a real world object. KineScan aims at providing an easy way to perform the opposite task of digitalizing a real object into a 3D model. 3D Scanning requires a depth sensor which typically outputs a depth stream, that can be then easily converted into a point cloud. This point cloud represents the object viewed from one side, which is usually not enough to construct a full model, so 3D scanners also need to move either the object or the sensor in order to capture multiple point clouds from different point of view and then combine them into the final model. 

KineScan solves the problem of the hardware requirement of the depth sensor with the Microsoft Kinect, a cheap device available globally and already owned by a lot of users. The goal of KineScan is also to make scanning as easy as possible, so the problem of aligning clouds was solved by placing the object on a \textsc{Lego} platform that rotates at a constant speed, allowing the KineScan software to merge clouds without help from the user.

The last step of every 3D scanner is to convert the final point cloud into a polygon mesh that can be used by 3D applications. KineScan solves this problem by creating Delaunay triangulations on subsets of points projected in their best fitting plane and then using the corresponding 3D triangles for the final mesh.

\subsection{Project Overview}
First of all, as shown as in Figure \ref{fig:overview} the Microsoft Kinect and the \textsc{Lego} NXT brick should be connected to the Windows PC. The object to scan should be on the \textsc{Lego} platform. The user can then run KineScan, which will show the scene as seen from the Kinect, and the user has to click on the rotation axis. KineScan will then compute the 3D position of it. After that the user has to again click, but on the object itself, so that KineScan can later remove the background. At this point KineScan will start the rotation of the \textsc{Lego} platform and it will save a point cloud every second. During this period of time the cloud will be rendered on screen. Once a full rotation of the object is completed, KineScan will build the triangle mesh, and finally it will generate the resulting \texttt{mesh.obj} file.

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{img/overview}
\caption{Overview}
\label{fig:overview}
\end{figure}

\subsection{Point Cloud}
A point cloud is a set of data points in a coordinate system. In KineScan all the point clouds are defined in a three-dimensional system, so each point of the cloud describes a point on the surface of the object with coordinates X, Y and Z. An example of a point cloud of a Stanford Bunny is shown in Figure \ref{fig:stanford_bunny_cloud}.

While point clouds can be rendered and inspected, usually point clouds are not directly usable in 3D application, because they do not directly provide information about the surface itself. In order to use the data in most application, point clouds have to be converted to polygon meshes, triangle meshes or other models, through a process called surface reconstruction.

\subsection{Triangle Mesh}
A triangle mesh is a collection of triangles that defines the shape of a polyhedral object in three dimensions. Each triangle in the mesh is identified by three vertices, while each vertex is identified by the three coordinates in the space. Very often, a vertex is shared between multiple triangles. An example of a triangle mesh of a Stanford Bunny is shown in Figure \ref{fig:stanford_bunny_mesh}.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{img/bunny_cloud.png}
	\caption{Point Cloud representation}
	\label{fig:stanford_bunny_cloud}
\end{subfigure}
\hfill
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.55\textwidth]{img/bunny_mesh.png}
	\caption{Triangle Mesh representation}
	\label{fig:stanford_bunny_mesh}
\end{subfigure}
\caption{Different representations of a Stanford Bunny}
\end{figure}

\subsection{KD-Tree}
A kd-tree, which stands for k-dimensional tree, is a space partitioning data structure for organizing points in a k-dimensional space. Kd-trees are a very useful data structure for searching points or set of points such as neighbours. The kd-tree is a binary tree in which every leaf node is a k-dimensional point and every non-leaf node is a hyperplane that divides the space into two parts. KineScan uses three-dimensional kd-trees (Figure \ref{fig:kdtree}), so every leaf node is a 3D point of the cloud, and every non-leaf node is a plane perpendicular to an axis. 

The octree is another popular space partitioning data structure in three dimensions. The main difference is that in an octree each internal node has exactly eight children, partitioning the space in eight parts, while in a kd-tree each internal node has only two children, because the space is partitioned in two parts. Another difference is that a cell in a kd-tree might have an high aspect ratio, while cells in an octree are guaranteed to be cubical.

Kd-trees are guaranteed to have logarithmic depth in the size of the input, so insertion, removal and search for the nearest neighbour of a point takes $O(\log n)$ time. Building a kd-tree can take as little as $O(n \log n)$ \cite{Cormen:1990:IA:80156}.

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{img/kdtree.png}
\caption{A 3 dimensional kd-tree}
\label{fig:kdtree}
\end{figure}

\subsection{Microsoft Kinect}

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{img/kinect.png}
\caption{Microsoft Kinect}
\label{fig:kinect}
\end{figure}

The Kinect (Figure \ref{fig:kinect}) is a motion sensor input device by Microsoft designed for the Xbox and Windows PC platforms. It enables users to control their system through gestures and spoken commands. The input capabilities of the Kinect can be divided in three distinct parts:

The depth sensor is actually made of two components in the Kinect: in the leftmost lens area there is an infrared laser projector, while in the rightmost one there is a monochrome sensor for the same kind of light. 

The depth recognition works similarly to the human eyes, but with a clever trick. Instead of using two cameras the Kinect uses an infrared projector which will draw a randomized speckle pattern on the scene (Figure \ref{fig:speckle_pattern}). The infrared camera will then detect such patterns in the scene, and since the Kinect knows the angle at which each group of speckles have been drawn, it can then triangulate the position of the points in the scene.

The depth stream is then encoded as an image with a resolution of 640 $\times$ 480 pixels, where each pixel contains a number representing the distance of the corresponding point from the Kinect sensor, or a sentinel value in case the depth was not correctly detected. Figure \ref{fig:raw_depth_stream} shows the raw depth stream, where colors represent the distance from the Kinect, and the white pixels are the ones where depth was not detected.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{img/kinect_depth_speckles.jpg}
	\caption{Kinect's speckle pattern seen with an IR camera}
	\label{fig:speckle_pattern}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{img/depth_raw.png}
	\caption{Raw depth image}
	\label{fig:raw_depth_stream}
\end{subfigure}
\caption{Depth stream of the Kinect sensor}
\end{figure}

The RGB sensor is the central lens in the Kinect. It is a simple camera that can run at 640 $\times$ 480 pixels at 30 frames per second or 1280 $\times$ 960 pixels at 12 frames per second. KineScan uses the first option since it does not need the extra resolution in the color stream.

The Kinect has an array of microphones in the front, enabling it to conduct acoustic source localization and ambient noise suppression. This feature is not used in KineScan.

\subsection{Lego NXT}
The \fnurl{\textsc{Lego} Mindstorms NXT}{http://mindstorms.lego.com} is a programmable robotics kit. The main component is a micro computer called Intelligent Brick (Figure \ref{fig:nxt_brick}) that can take input from up to four sensor and control up to three motors (Figure \ref{fig:nxt_motor}) using a proprietary cable. The motors are all alike, but there are different sensors:
\begin{itemize}
\item Touch sensor detects when the bump is pressed or released.
\item Light sensor detects the light level, and also contain a LED for illuminating an area.
\item Sound sensor measures the ambient volume.
\item Ultrasonic sensor measures the distance from the sensor to the closest wall or obstacle.
\end{itemize}

The intelligent brick can be programmed in many ways. \textsc{Lego} provides a SDK and an environment to create, upload to the brick and run programs. Thanks to the SDK, third parties have developed similar tools to create programs. 

Additionally, it is possible to directly issue commands to the intelligent brick via bluetooth or USB, and this can be used to control the NXT system in real time using most common programming languages.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{img/nxt_brick.png}
	\caption{Lego NXT Intelligent Brick}
	\label{fig:nxt_brick}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.6\textwidth]{img/nxt_motor.png}
	\caption{Lego NXT Motor}
	\label{fig:nxt_motor}
\end{subfigure}
\caption{Lego NXT System}
\end{figure}

\newpage

\section{State of the Art}
The first bit project on using the Kinect as a 3D scanner is KinectFusion by Microsoft Research \cite{export:155416, export:155378}. KinectFusion enables a user holding and moving a standard Kinect camera to rapidly create detailed 3D reconstructions of an indoor scene. The depth data is used both to track the position of the sensor and to construct the 3D model of the scene.

After KinectFusion, two commercial projects were made: \fnurl{ReconstructMe}{http://reconstructme.net/} and \fnurl{KScan3D}{http://www.kscan3d.com/}. Both those projects have features similar to KinectFusion, and they can also create coloured scenes using the color stream of the Kinect together with the depth data.

It is important to note that the goal of those projects was to build a 3D model of the whole scene by moving the Kinect sensor around the scene itself while the software would compute its relative position. KineScan uses a different idea of having the Kinect sensor in a fixed position and moving the object in front of it while removing the background.

A different approach has been taken in the work of Rusinkiewicz, Hall-Holt and Levoy \cite{Rusinkiewicz:2002:RMA:566654.566600}. They do not use a Kinect sensor, but they have a setup which is surprisingly similar to it: they use a DLP projector and a NTSC video camera. The projector sends a light stripe, and the camera recognizes it, triangulating the position of the object. Similarly to KineScan, their system has the projector and camera fixed, while the object can be moved freely in front of them, but unlike KineScan, they use a variation of the ICP algorithm to align point clouds, so the object can be moved by hand and scanned incrementally from all directions.

\clearpage


\section{Project Requirement and analysis}

\subsection{Challenges}
The goal of the software part of the project is to convert the raw Kinect data into a point cloud, then combine multiple point clouds into one, and lastly build a triangle mesh. This process poses some challenges:

\begin{itemize}
\item Removing the background from the point cloud. We only want the point cloud of the object, so we need to be able to distinguish it.
\item Building the triangle mesh. This process, also called surface reconstruction, is highly complex. KineScan simplifies this process by using 2D approximations of subsets of points.
\end{itemize}

\subsection{Programming Language}
Early in the design phase I had to choose the programming language I was going to use for the project. The first constraint was imposed by the Kinect, since the official Microsoft Kinect SDK only supports C++, C\# and VB.NET. 

Between those three, I only had experience in C++, but at the end I decided to use C\# because it is simpler to use and maintain, and more similar to Java, in which I have a lot of experience on.

\subsection{Technologies and Libraries}
As previously noted, the \fnurl{Microsoft Kinect SDK}{http://www.microsoft.com/en-us/kinectforwindowsdev/Start.aspx} is needed to use the Kinect efficiently. The SDK includes very helpful tutorials and examples in the three supported languages, and it is very well documented.

Microsoft XNA is a set of tools with a managed runtime environment provided by Microsoft that facilitates video game development for Windows, Windows Phone and Xbox. I decided to use XNA since it provides an easy to use graphics wrapper for both 2D and 3D visualizations, and it also gives a good structure to the project, in the form of initialization, update and draw methods.

\fnurl{MindSqualls}{http://www.mindsqualls.net/} is a library for remote controlling Lego Mindstorms Nxt bricks via bluetooth or USB connection. I've chosen this library for its simplicity of use, as for example connecting to the brick and running a motor would take only four simple lines of code as shown in Listing \ref{lst:mindsqualls_example}.

\vspace{15pt}
\begin{lstlisting}[caption={MindSqualls sample code},label={lst:mindsqualls_example}]
NxtBrick brick = new NxtBrick(NxtCommLinkType.USB, 0);
NxtMotor motor = new NxtMotor();
brick.MotorA = motor;
motor.Run(50, 0);
\end{lstlisting}


\fnurl{ALGLIB}{http://www.alglib.net/} is a cross-platform numerical analysis and data processing library. It supports several programming languages and operating systems. The library features data analysis, optimization, interpolation, linear algebra and much more, KineScan uses ALGLIB for building the kd-tree of the point cloud, finding neighbours of a given point, and for some linear algebra such as finding covariance matrix, eigenvalues and eigenvectors.

\fnurl{Triangle.NET}{http://triangle.codeplex.com/} is a C\# port of Jonathan Richard Shewchuk's Triangle \cite{shewchuk96b} software that generates 2D Delaunay triangulations and high-quality meshes of point sets.

\subsection{Geometry definition file format}
In order to export the final triangle mesh, I needed to choose a file format. Initially I opted for the OBJ file, then I moved on the PLY format in order to support vertex colors.

\subsubsection{OBJ File Format}
The \fnurl{OBJ file format}{http://en.wikipedia.org/wiki/Wavefront_.obj_file} was developed by Wavefront Technologies, but is now open and used by most 3D graphics applications. It is very simple, each line contains a letter that identifies the content of the line such as ``v'' for vertex or ``f'' for face, and then values separated by a space character. Vertices are referred with their index in the file in the face declarations. It is also possible to define texture coordinates and normals but it is not possible to define a rgb color for each vertex.

\begin{figure}[h]
	\begin{minipage}{0.48\textwidth}
		\begin{center}
			\begin{minipage}{0.5\textwidth}
				\lstinputlisting{src/cube.obj}
				\label{lst:cube_obj}
			\end{minipage}					
			\caption{Simple Obj file representing a cube}
		\end{center}			
	\end{minipage}
\hfill
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[height=0.6\textwidth]{img/cube1.png}
		\caption{Rendered cube}
		\label{fig:cube_obj}
	\end{minipage}
\end{figure}

\clearpage


\subsubsection{PLY - Polygon File Format}
The \fnurl{Polygon File Format}{http://paulbourke.net/dataformats/ply/} is intended to describe a collection of vertices, faces and other elements. It is very versatile, since application can create new properties attached to elements of an object. For example, the properties red, green and blue are often associated with vertex elements.

The file is divided in two parts, first there is an header, which contains the number of elements and their properties, and then there is the list of elements, usually vertices and faces.

\begin{figure}[h]
	\begin{minipage}{0.48\textwidth}
		\begin{center}
			\begin{minipage}{0.8\textwidth}
				\lstinputlisting{src/cube.ply}
				\label{lst:cube_ply}
			\end{minipage}					
			\caption{Simple Ply file representing a cube}
		\end{center}			
	\end{minipage}
\hfill
	\begin{minipage}{0.48\textwidth}
		\centering
		\includegraphics[height=0.6\textwidth]{img/cube2-small.png}
		\caption{Rendered cube}
		\label{fig:cube_ply}
	\end{minipage}
\end{figure}

\clearpage

\section{Project Design}

\subsection{Lego Platform}

One of the first tasks of the project was to actually build the \textsc{Lego} platform, with a few important requirements:
\begin{itemize}
    \item \textbf{Stable} in order not to make the object oscillating while rotating.
    \item \textbf{Robust} to hold relatively heavy objects.
    \item \textbf{Simple} because I had a limited number of \textsc{Lego} pieces.
    \item The rotation of the platform must be \textbf{slow} and \textbf{steady}
\end{itemize}

Following these requirements, I built the \textsc{Lego} platform (Figure \ref{fig:lego_platform}) with a compact and simple mechanism, where the motor directly runs a worm gear, to ensure the steadiness of the rotation. The rotation axis is visually represented by a green cylinder under the platform.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/legoplatform.png}
\caption{Lego Platform}
\label{fig:lego_platform}
\end{figure}

\subsection{Pipeline overview}

\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{img/pipeline.pdf}
\caption{Pipeline}
\label{fig:pipeline}
\end{figure}

As shown in Figure \ref{fig:pipeline}, the project is divided into distinct units of work, and each one of them is represented by a different C\# class. 
\begin{itemize}
\item Find rotation axis
\item Find Object
\item Start the rotation and capture point clouds
\item Combine the point clouds
\item Create the triangle mesh
\end{itemize}

Each state is a subclass of \texttt{ScannerState} and contains the methods \texttt{update()}, \texttt{nextState()} and \texttt{draw()}. The main routine will keep track of the current state, and it will call those methods repeatedly. 



\subsection{Find the rotation axis}
This is the first step of the project. The user has to click in the GUI on the rotation axis, represented in the \textsc{Lego} platform by the green cylinder as shown in Figure \ref{fig:findrotationaxisgui}. KineScan will then map the RGB image to the depth image and find the position of the point in the coordinates system with the Kinect at the origin. This point will also be offset by a small amount to actually find the rotation axis, due to the physical thickness of the object.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/clickrotationaxisgui2.png}
\caption{Find the rotation axis - user interface}
\label{fig:findrotationaxisgui}
\end{figure}

\subsection{Find the object}
This step is similar to the previous one. Later in the pipeline we will need to extract the object from its surroundings, this process requires a starting point, which is what we are looking for in this state. As shown in Figure \ref{fig:findobjectgui}, the user once again has to click somewhere on the object, ideally on a point that will always belong to the object during the rotation. Differently from the previous state, here we only have to find the 2D screen coordinates of the clicked point. It is not as trivial as finding the X and Y coordinates of the mouse though, because the coordinates should be in the coordinates system of the depth camera, not the RGB camera. The user clicks on an image taken by the RGB camera, so the coordinates have to be mapped into the other coordinate system.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/clickobjectgui.png}
\caption{Find the object - user interface}
\label{fig:findobjectgui}
\end{figure}

\subsection{Save the point clouds}
At this point we can finally start rotating the \textsc{Lego} platform. We know by how much the object rotated because the rotation speed is known, as well as the time-stamp of when it started, and the current time.

The point cloud is constructed easily from the depth data, but it is important to remove the background from the point cloud. This is done during the cloud creation itself in the \texttt{PointCloudFactory}. This process uses a Vertex data structure containing both the 3D world coordinates of the point and the 2D screen coordinates. The basic idea is to use screen coordinates to find neighbours and expand the search, while using world coordinates to actually compare distances.

The starting point is given by the previously described \texttt{FindObjectState}, we add that point to a queue, then for each point of the queue we add its 3D representation to the final cloud, and then we take all 8 screen coordinates neighbours: for each neighbour we check their world coordinate distance to the point, if that is more than a threshold, we discard it, otherwise we add the neighbour in the queue. Whenever we add a new data point to the cloud, it is also important to find its color: in order to do so we need to find the screen coordinates of the pixel correspondent to the current data point, and then extract the color values from the RGB image.

Once the point cloud of the current view of the object is created, we translate all the points by the distance of the rotation axis, so rotation axis will correspond to the y-axis of the coordinate system. Finally, we rotate the cloud around the y-axis by the known rotation.

\subsection{Combine point clouds}
Since the clouds were rotated already in the previous step, they are already aligned, so the combination of point clouds is simply a point cloud with all the previous ones added into it.

\subsection{Construct Triangle Mesh}
At this point we have to perform surface reconstruction and save the resulting triangle mesh to file. The technique used for surface reconstruction is similar to the one described by Gopi, Krishnan and Silva \cite{journals/cgf/GopiKS00} which consists in using a lower dimensional localized Delaunay triangulation. The main difference in KineScan is that I do not consider normals and my candidate points are just the neighbours.

More specifically, after building a kd-tree, the algorithm performs the following for each point $p$:

\begin{itemize}
\item Find the closest $n$ neighbours $N$ using alglib and the previously built kd-tree, where $n$ is arbitrarily chosen to be 12.
\item Find the average 3D point $a$ of the neighbours and the starting point.
$$a = \frac{\sum\limits_{i=1}^n{N_i} + p}{n + 1}$$

\item Build a matrix $M$ with size $3 \times (n+1)$ where each column is a point, the first point being $p$ and the next one are the neighbours.
$$
M_{3 \times (n+1)} = 
\begin{pmatrix}
p & N_1 & N_2 & \cdots & N_n \\
\end{pmatrix}
$$

\item Subtract the average to each point, so that the average of the small collection of points will be at the origin of the coordinate system.
$$M_i = M_i - a$$
such that:
$$\sum\limits_{i=1}^{n+1}{M_i} = \begin{pmatrix} 0 \\ 0 \\ 0 \\ \end{pmatrix}
$$

\item Compute covariance matrix $C$
$$C = M*M^T$$

\item Find eigenvalues and eigenvectors of $C$ using alglib, then build a matrix $P$ with size $3 \times 2$ that contains as columns the two eigenvectors correspondent to the two biggest eigenvalues.

\item Compute the least square plane matrix of $M$ and $P$ using alglib. This matrix will have size $2 \times (n+1)$ and each column will be a 2D point which is the projection the correspondent point of $M$ into the best fitting plane.

\item Remove any 2D duplicate points.

\item Compute the delaunay triangulation for the 2D points using Triangle.NET.

\item Considering the 2D correspondent of the original 3D point, get the first ring of triangles and save them for the final mesh.
\end{itemize}

\section{Implementation issues}

\subsection{Finding the Rotation Axis}

Initially, the idea was to automatically find the rotation axis using a green colored part along the axis of the \textsc{Lego} platform. The issue was that the Kinect has a poor color definition, and it often displays green pixels even where there is no green in the scene, often along borders of objects. Since there is no additional information, it is impossible to distinguish which group of green pixel is the right one.

\subsection{Kinect Data Structures}
The Kinect color stream has a few oddities. First of all, the Kinect is supposed to be used in front of the user, so the image is flipped left-to-right If we want to use it properly as a camera, we need to flip the image back in the x axis, otherwise we must take special care when detecting mouse clicks, as we need to flip them instead.

Another oddity of the Kinect color stream is that each pixel is represented by 4 bytes representing the blue, green and red colors, while the last byte is empty. This BGR representation is unusual, and we need to convert the data into the more common RGB representation swapping each first and third bytes of the color stream.

\newpage

\section{Results}
I will present a few real examples. The first image of each set has been taken by the Kinect color stream, the point cloud is a screen of the KineScan application, and the meshes were imported and rendered in \fnurl{Blender}{http://www.blender.org/}.

The cube is a regular solid bounded by six square faces. This cube in particular has a pattern on each side which was approximately captured in the 3D scan.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cubeplatform.png}
	\caption{Cube on platform}
	\label{fig:cubeplatform}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cubecloud.png}
	\caption{Point Cloud}
	\label{fig:cubecloud}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cubemesh1.png}
	\caption{Triangle Mesh}
	\label{fig:cubemesh1}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cubemesh2.png}
	\caption{Smoothed Triangle Mesh}
	\label{fig:cubemesh2}
\end{subfigure}
\caption{Cube Scan}
\label{fig:cubescan}
\end{figure}

\clearpage

The cuboctahedron is a polyhedron with eight triangular faces and six square faces. This cuboctahedron is also painted with a pattern which is the same for every kind of face, but with different colors.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cuboctahedronplatform.png}
	\caption{Cuboctahedron on platform}
	\label{fig:cuboctahedronplatform}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cuboctahedroncloud.png}
	\caption{Point Cloud}
	\label{fig:cuboctahedroncloud}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cuboctahedronmesh1.png}
	\caption{Triangle Mesh}
	\label{fig:cuboctahedronmesh1}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/cuboctahedronmesh2.png}
	\caption{Smoothed Triangle Mesh}
	\label{fig:cuboctahedronmesh2}
\end{subfigure}
\caption{Cuboctahedron Scan}
\label{fig:cuboctahedronscan}
\end{figure}

\clearpage

The teddy bear is a complex object, with fur and cloth, but KineScan is still able to detect most features of the object as well as most colors. There are a few issues in the legs because the Kinect sensor can hardly see those parts during the rotation.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearreal.png}
	\caption{The Teddy Bear}
	\label{fig:bear}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearonplatform.png}
	\caption{Bear on Platform}
	\label{fig:bearonplatform}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearcloud.png}
	\caption{Point Cloud}
	\label{fig:bear_cloud}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearmesh1.png}
	\caption{Triangle Mesh}
	\label{fig:bear_mesh1}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearmesh2.png}
	\caption{Smoothed Triangle Mesh}
	\label{fig:bear_mesh2}
\end{subfigure}
\begin{subfigure}[b]{0.4\textwidth}
	\centering
	\includegraphics[height=0.85\textwidth]{img/bearmesh3.png}
	\caption{Rendered Triangle Mesh}
	\label{fig:bear_mesh3}
\end{subfigure}
\caption{Teddy Bear Scan}
\label{fig:bearscan}
\end{figure}

\clearpage

\subsection{Limitations}
\subsubsection{Big Objects}
Heavy or big objects cannot be scanned with the current \textsc{Lego} base due to its size. Big objects would just fall off. This could be solved building a larger base with more \textsc{Lego} pieces.

\subsubsection{Small Objects}
The depth detection of the Microsoft Kinect fails with points closer than about a meter or farther than four. Scanning small objects can be difficult because they still have to be put about a meter away from the sensor, so the effective resolution will be really small and the noise in the image will be amplified a lot. This issue can be partially solved using special lenses that enlarge the field of view and reduce the minimum distance.

\subsubsection{Kinect, Sunlight and infrared light}
The Microsoft Kinect uses an infrared projector, so direct sunlight on the scene can totally ruin the depth detection and the scan. Moreover, other devices that create powerful infrared light on the scene can be harmful, even though there are not many common devices capable of this. Of course, multiple Kinects pointing towards the same area will ruin each other's depth perception, since the infrared projections will overlap.

\subsubsection{Depth Image Noise}
As shown previously in Figure \ref{fig:raw_depth_stream}, the depth image of the Kinect is unfortunately really noisy and trembling. This effect is mostly evident when scanning objects with a lot of small features. The issue comes from the way that the depth perception works: the Kinect projects an infrared speckles pattern on the scene, and it needs to recognize a group of speckles in order to triangulate the depth. If a feature of an object is too small and not enough speckles will fit, then the Kinect will not be able to detect the depth well. 

This issue has been solved with the new Kinect (version 2) since it uses a completely different way of detecting the depth. For each pixel of the scene it now sends a brief light, and measures the time it takes for the light to travel back to the camera. This process is mostly independent for each pixel, so the final image is much more precise. As an example, the new Kinect will be able to reliably track fingers, while the current one can't.

\subsubsection{Depth image resolution}
The Microsoft Kinect supports a maximum depth resolution of 640 $\times$ 480 pixels. The resolution directly affects the number of points in the clouds, and on a normal scan, the maximum resolution gives a number of points in the order of $10^5$. Using this many points, surface reconstruction might take up to an hour on an high-end desktop pc. For this reason, I decided to reduce the base definition to 320 $\times$ 240. Halving the resolution in both dimensions the number of points in the cloud greatly decreases of approximately two orders of magnitude, and the surface reconstruction can be completed in a few minutes. It is important to note that the whole project still supports the maximum resolution, which can be used to construct higher definition point clouds without surface reconstruction.

\subsubsection{Mesh Holes}
The final mesh will have at least one major hole at the bottom, since the bottom part of the object is obviously not scanned. Often there will also be a hole on the top, if the Kinect sensor was looking at the object from the side. An example is shown in Figure \ref{fig:cuboctahedronholes}, which is a rendering of the cuboctahedron from Figure \ref{fig:cuboctahedronscan} viewed from top. It is evident how the whole top faces is missing, because the object was scanned from the side, and the Kinect sensor could not see it. Moreover, there is a hole in the bottom, where the object touches the platform.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{img/cuboctahedronholes.png}
\caption{Holes in the cuboctahedron (Viewed from the top)}
\label{fig:cuboctahedronholes}
\end{figure}

\subsection{Future work or possible developments}

There are many possibilities for improvements or future work.

\subsubsection{Improving the removal of background or platform}
The algorithm used to construct point clouds of the object only is based on the world distance of data points. Sometimes this algorithm either fails to remove a bit of the \textsc{Lego} platform or it removes parts of the object that are too far from the starting point. The algorithm could be improved to fix those issues, and there could be a later phase where the program removes the platform, either based on y-coordinates of data points or considering points that do not move during the rotation.

\subsubsection{Capturing point clouds}
KineScan currently saves a point cloud every second, a better strategy would consist in actively choosing the best frame to capture, maybe based on the number of points detected in the cloud. Due to the noisy depth stream of the Kinect sensor, it could also be possible to stop the platform, capture multiple frames and average them in order to achieve better quality.

\subsubsection{Merging point clouds}
Since we know the exact rotation speed, merging the clouds is a simple operation where they are added on top of each other. Combining clouds with techniques such as the ICP algorithm \cite{Besl:1992:MRS:132013.132022, Horn87closed-formsolution, Rusinkiewicz:2001:EVO, Gelfand03geometricallystable} should not be necessary but they could improve the result, and the process might be further improved by removing overlapping or close points.

\subsubsection{Mesh Smoothing}
The point cloud could be greatly improved by smoothing surfaces. The examples shown previously have been smoothed by \fnurl{Blender}{http://www.blender.org}, which works by flattening the angles between adjacent faces in the mesh. Blender's smoothing is a relatively simple feature, and more advanced techniques could also be used. \cite{Jones:2003:NFM:1201775.882367, Desbrun:1999:IFI:311535.311576}

\subsubsection{Surface Reconstruction}
KineScan currently builds the triangle mesh considering every point of the cloud, taking its neighbours, projecting them in 2D, and computing the delaunay triangulation of those 2D points. This way often results in jagged meshes, especially with the noisy nature of the starting cloud. The algorithm could be improved by doing the surface reconstruction directly on 3D data points \cite{Gopi:2002:FEP:646016.678279, giraudot:hal-00844472, digne:hal-00827623}.


\bibliographystyle{plain}
\bibliography{references}{}

\end{document}




















