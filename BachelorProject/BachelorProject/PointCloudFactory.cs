﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {

    /*
     * A vertex in the point cloud, containing both 3d information (x,y,z) and pixel x,y information
     * This class is only used for cloud construction
     */
    public struct VertexPixel {
        public float X;
        public float Y;
        public float Z;
        public int PixelX;
        public int PixelY;

        public VertexPixel(float x, float y, float z, int pixelX, int pixelY) {
            this.X = x;
            this.Y = y;
            this.Z = z;
            this.PixelX = pixelX;
            this.PixelY = pixelY;
        }
    }

    /*
     * A factory class for point clouds
     */
    public class PointCloudFactory {

        // Generate an empty point cloud
        public static PointCloud emptyPointCloud() {
            return new PointCloud();
        }

        // Generate a PointCloud with all the points given. Mainly used for testing purposes.
        public static PointCloud rawPointCloud(int width, int height, SkeletonPoint[] skeletonPoints) {
            PointCloud cloud = new PointCloud(width, height);
            foreach (SkeletonPoint p in skeletonPoints) {
                cloud.add(new CloudVertex(new Vector3(p.X, p.Y, p.Z), Color.Black));
            }
            return cloud;
        }

        // Generate a PointCloud containing only the points near the center one
        public static PointCloud reducedPointCloud(int width, 
            int height, 
            SkeletonPoint[] skeletonPoints, 
            ColorImageFrame colorFrame, 
            CoordinateMapper mapper, 
            Point objectLocation) {

            const float DISTANCE_CHECK = 0.005f; // In meters. A point is in the cloud if has less than this distance to any other point in the cloud.

            // Create data structures
            PointCloud cloud = new PointCloud();
            Queue<VertexPixel> queue = new Queue<VertexPixel>();
            bool[] visited = new bool[width * height];

            byte[] colorData = colorFrame.GetRawPixelData();

            int startX = objectLocation.X;
            int startY = objectLocation.Y;
            int startIndex = startY * width + startX;
            VertexPixel start = new VertexPixel(-skeletonPoints[startIndex].X, skeletonPoints[startIndex].Y, skeletonPoints[startIndex].Z, startX, startY);
            queue.Enqueue(start);
            visited[startIndex] = true;

            while (queue.Count != 0) {
                VertexPixel current = queue.Dequeue();

                // Get color data
                ColorImagePoint colorImagePoint = mapper.MapSkeletonPointToColorPoint(skeletonPoints[current.PixelY * width + current.PixelX], Scanner.COLOR_FORMAT);
                int colorIndex = colorImagePoint.Y * colorFrame.Width + colorImagePoint.X;
                int r = colorData[colorIndex * colorFrame.BytesPerPixel + 2];
                int g = colorData[colorIndex * colorFrame.BytesPerPixel + 1];
                int b = colorData[colorIndex * colorFrame.BytesPerPixel + 0];

                cloud.add(new CloudVertex(new Vector3(current.X, current.Y, current.Z), new Color(r, g, b)));

                // Loop over the 8 neighbors
                for (int y = current.PixelY - 1 > 0 ? current.PixelY - 1 : 0; y <= current.PixelY + 1 && y < height; ++y) {
                    for (int x = current.PixelX - 1 > 0 ? current.PixelX - 1 : 0; x <= current.PixelX + 1 && x < width; ++x) {
                        // No need to check for the center, because the current node is visited.
                        int index = y * width + x;
                        if (!visited[index]) {
                            if (Math.Abs(skeletonPoints[index].Z - current.Z) < DISTANCE_CHECK) {

                                queue.Enqueue(new VertexPixel(-skeletonPoints[index].X, skeletonPoints[index].Y, skeletonPoints[index].Z, x, y));
                                visited[index] = true;
                            }
                        }
                    }
                }
            }
            return cloud;
        }
    }
}
