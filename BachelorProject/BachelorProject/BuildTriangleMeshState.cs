﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TriangleNet.Geometry;
using TriangleNet;
using TriangleNet.Data;

namespace KineScan {
    /*
     * The state of the scanner where we do surface reconstruction
     */
    class BuildTriangleMeshState : ScannerState {

        private PointCloud pointCloud;
        private BasicEffect effect;
        private ViewHandler viewHandler;

        // Write-to-file stuff
        private FileStream fStream;
        private BufferedStream bStream;
        private TextWriter writer;
        private int writeCount; // counts the number of times the file was written before a flush.

        public BuildTriangleMeshState(KinectSensor kinect,
            GraphicsDevice graphicsDevice,
            SpriteBatch spriteBatch,
            SpriteFont font,
            PointCloud pointCloud,
            BasicEffect effect,
            ViewHandler viewHandler)

            : base(kinect, graphicsDevice, spriteBatch, font) {

            this.pointCloud = pointCloud;
            this.effect = effect;
            this.viewHandler = viewHandler;

            // If you don't want to build the triangle mesh, just return here, and it will skip that (and it will still render the full point cloud)
            return;

            // Open/create the file
            fStream = new FileStream("mesh.ply", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            bStream = new BufferedStream(fStream);
            writer = new StreamWriter(bStream);
            writeCount = 0;

            Console.WriteLine("# Now in BuildTriangleMeshState - Please wait while the triangle mesh is being generated");

            // build data structures for alglib. Both a rectangular array and a jagged array are needed for simplicity later.
            const int spaceDimensions = 3;
            double[,] datasetRec = new double[pointCloud.Length, spaceDimensions];
            double[][] datasetJag = new double[pointCloud.Length][];
            Color[] colors = new Color[pointCloud.Length]; // there are 3 colors: r, g, b
            int[] indexes = new int[pointCloud.Length];
            {
                int i = 0;
                foreach (CloudVertex v in pointCloud) {
                    indexes[i] = i;
                    colors[i] = v.color;
                    datasetRec[i, 0] = v.vector.X;
                    datasetRec[i, 1] = v.vector.Y;
                    datasetRec[i, 2] = v.vector.Z;
                    datasetJag[i] = new double[] { v.vector.X, v.vector.Y, v.vector.Z };
                    ++i;
                }
            }

            writeLine("ply");
            writeLine("format ascii 1.0");
            writeLine("Comment Author: Giorgio Gori");
            writeLine("element vertex " + pointCloud.Length);
            writeLine("property float x");
            writeLine("property float z"); // why is it x z y instead of x y z? A PLY mystery...
            writeLine("property float y");
            writeLine("property uchar red");
            writeLine("property uchar green");
            writeLine("property uchar blue");
            writeLine("element face 0");
            writeLine("property list uchar int vertex_index");
            writeLine("end_header");
            for (int i = 0; i < pointCloud.Length; ++i) {
                writeLine(datasetRec[i, 0] + " " + datasetRec[i, 1] + " " + datasetRec[i, 2] + " " + colors[i].R + " " + colors[i].G + " " + colors[i].B);
            }

            // build the kdtree
            alglib.kdtree kdtree;
            alglib.kdtreebuildtagged(datasetRec, indexes, spaceDimensions, 0, 0, out kdtree);

            const int numberOfNeighbors = 12;
            const int numberOfPoints = numberOfNeighbors + 1;

            int facesCount = 0;

            // for for each point...
            for (int i = 0; i < pointCloud.Length; ++i) {

                if (i % 1000 == 0) {
                    Console.WriteLine(i + " / " + pointCloud.Length);
                }

                double[] centerPoint = datasetJag[i];

                // find closest neighbors
                alglib.kdtreequeryknn(kdtree, centerPoint, numberOfNeighbors, false);
                int[] tagResults = new int[numberOfNeighbors];
                alglib.kdtreequeryresultstags(kdtree, ref tagResults);

                // average them
                double[] average = new double[] { 0, 0, 0 };
                for (int dim = 0; dim < spaceDimensions; ++dim) {
                    average[dim] += centerPoint[dim];
                    for (int t = 0; t < numberOfNeighbors; ++t) {
                        average[dim] += datasetJag[tagResults[t]][dim];
                    }
                    average[dim] /= numberOfPoints;
                }

                // put the points in a matrix, subtract their average
                double[,] matrixM = new double[numberOfPoints, spaceDimensions];
                for (int dim = 0; dim < spaceDimensions; ++dim) {
                    matrixM[0, dim] = centerPoint[dim] - average[dim];
                    for (int t = 0; t < numberOfNeighbors; ++t) {
                        matrixM[t + 1, dim] = datasetJag[tagResults[t]][dim] - average[dim];
                    }
                }

                // compute covariance matrix
                double[,] covMatrix = new double[spaceDimensions, spaceDimensions];
                alglib.covm(matrixM, out covMatrix);
                //printMatrix(covMatrix, "Covariance matrix");

                // compute eigenvalues and eigenvectors
                double[] eigVal;
                double[,] eigVec = new double[spaceDimensions, spaceDimensions];
                alglib.smatrixevd(covMatrix, spaceDimensions, 1, true, out eigVal, out eigVec);
                //printMatrix(eigVec, "Eigen Vectors");

                // put the 2 eigen vectors correspondent to the 2 biggest eigenvalues in a matrix
                // - find index of minimum eigenvalue first
                int minEigValIndex = 0;
                for (int k = 1; k < eigVal.Length; ++k) {
                    if (eigVal[k] < eigVal[minEigValIndex]) {
                        minEigValIndex = k;
                    }
                }
                double[,] matrixP = new double[spaceDimensions, 2];
                // - and now add to the matrix the eigenvectors not corrispondent to the min eigenvalue
                {
                    int index = 0;
                    for (int k = 0; k < eigVal.Length; ++k) {
                        if (k != minEigValIndex) {
                            for (int dim = 0; dim < spaceDimensions; ++dim) {
                                matrixP[dim, index] = eigVec[dim, k];
                            }
                            ++index;
                        }
                    }
                }
                //printMatrix(matrixP, "Matrix P");

                // compute least square plane matrix
                double[,] lsqm = new double[numberOfPoints, 2];
                {
                    int m = matrixM.GetLength(0);
                    int n = matrixP.GetLength(1);
                    int k = matrixM.GetLength(1);
                    alglib.rmatrixgemm(m, n, k, 1, matrixM, 0, 0, 0, matrixP, 0, 0, 0, 0, ref lsqm, 0, 0);
                }
                //printMatrix(lsqm, "lsqm");

                List<int> toSkip = new List<int>();
                for (int k = 0; k < numberOfPoints; ++k) {
                    for (int l = k + 1; l < numberOfPoints; ++l) {
                        if (sqrtDistance(lsqm[k, 0], lsqm[k, 1], lsqm[l, 0], lsqm[l, 1]) < 1e-8) {
                            toSkip.Add(k);
                            continue;
                        }
                    }
                }
                toSkip.Sort();
                toSkip.Add(numberOfPoints + 42); // the list toSkip needs 1 more number, greater than numberOfPoints.

                // create geometry for delaunay triangulation
                Mesh mesh = new Mesh();
                InputGeometry geometry = new InputGeometry(numberOfNeighbors);
                int skipIndex = 0;
                for (int p = 0; p < numberOfPoints; ++p) {
                    if (p == toSkip[skipIndex]) {
                        skipIndex++;
                    } else {
                        geometry.AddPoint(lsqm[p, 0]*1000, lsqm[p, 1]*1000);
                    }
                }
                // compute delaunay triangulation
                try {
                    mesh.Triangulate(geometry);
                } catch (Exception e) {
                    Console.WriteLine("Something went wrong, skipping");
                    Console.WriteLine(e.ToString());
                    continue; // This is horrible, and I'm sorry, but I have no idea on why sometimes it crashes
                }

                // make sure that the first point is the first point of the matrix
                //TriangleNet.Data.Vertex v = mesh.Vertices.First<TriangleNet.Data.Vertex>();
                //Console.WriteLine(v.X + " should be equal to " + lsqm[0, 0]);
                //Console.WriteLine(v.Y + " should be equal to " + lsqm[0, 1]);

                // Get the first ring
                ICollection<Triangle> triangles = mesh.Triangles;
                foreach (Triangle triangle in triangles) {
                    if (triangle.P0 == 0 || triangle.P1 == 0 || triangle.P2 == 0) {

                        // then the triangle has to be taken.
                        // Find indexes of the vertices
                        // triangle.P0 is the index in the delaunay, but also in the 2x13 matrix and in the 3x13 matrix
                        // Note that if the index is 0, then it is the original point, otherwise the point is to be found in tagResults.
                        // Also note that the obj file is indexed starting from one, so we have to +1 each index;
                        // Also note that the ply file is indexed starting from zero, so we don't have to +1 each index any more.

                        int i0 = (triangle.P0 == 0 ? i : tagResults[triangle.P0 - 1]);
                        int i1 = (triangle.P1 == 0 ? i : tagResults[triangle.P1 - 1]);
                        int i2 = (triangle.P2 == 0 ? i : tagResults[triangle.P2 - 1]);

                        // save the face
                        writeLine("3 " + i0 + " " + i1 + " " + i2);
                        ++facesCount;
                    }
                }
            }

            Console.WriteLine("Done");
            Console.WriteLine("there are " + facesCount + " faces!"); // This is important! It must be written in the ply file!

            writer.Close();
            bStream.Close();
            fStream.Close();

            Console.WriteLine("Done"); // for real
        }

        public override void update(GameTime gameTime) {
            effect.View = viewHandler.updateViewport();
        }

        public override void draw(GameTime gameTime) {
            graphicsDevice.Clear(Color.White);

            foreach (EffectPass effectPass in effect.CurrentTechnique.Passes) {
                effectPass.Apply();
                graphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, pointCloud.getDrawableList(), 0, pointCloud.Length, VertexPositionColor.VertexDeclaration);
            }
        }

        public override ScannerState nextState() {
            return this;
        }

        /*
         * Write a line of text in the file, and flush if needed
         */
        private void writeLine(string s) {
            writer.WriteLine(s);

            ++writeCount;
            if (writeCount > 1000) {
                writeCount = 0;
                writer.Flush();
                bStream.Flush();
                fStream.Flush();
            }
        }

        /*
         * Returns the squared distance between two points
         */
        private double sqrtDistance(double x1, double y1, double x2, double y2) {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        }
    }
}
