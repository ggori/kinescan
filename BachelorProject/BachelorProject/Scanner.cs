using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Kinect;
using NKH.MindSqualls;

namespace KineScan {
    /// This is the main type for your game
    public class Scanner : Microsoft.Xna.Framework.Game {

        public const DepthImageFormat DEPTH_FORMAT = DepthImageFormat.Resolution320x240Fps30;
        public const ColorImageFormat COLOR_FORMAT = ColorImageFormat.RgbResolution640x480Fps30;
        public const int WINDOW_WIDTH = 640;
        public const int WINDOW_HEIGHT = 480;

        private ScannerState currentState; // current scanner state
        private GraphicsDeviceManager graphicsDeviceManager; // XNA stuff

        public Scanner() {
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        protected override void Initialize() {
            // Kinect
            KinectSensor kinect = KinectSensor.KinectSensors[0];
            kinect.DepthStream.Enable(DEPTH_FORMAT);
            kinect.ColorStream.Enable(COLOR_FORMAT);
            kinect.Start();
            kinect.ElevationAngle = 0;

            // Graphic stuff
            SpriteBatch spriteBatch = new SpriteBatch(GraphicsDevice);

            // Window settings
            graphicsDeviceManager.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphicsDeviceManager.PreferredBackBufferHeight = WINDOW_HEIGHT;
            graphicsDeviceManager.IsFullScreen = false;
            graphicsDeviceManager.ApplyChanges();
            Window.Title = "KineScan";
            this.IsMouseVisible = true;

            SpriteFont font = Content.Load<SpriteFont>("font");

            currentState = new FindRotationAxisState(kinect, GraphicsDevice, spriteBatch, font);

            // XNA stuff
            base.Initialize();
        }

        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        protected override void LoadContent() {
        }

        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        protected override void UnloadContent() {
        }

        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        protected override void Update(GameTime gameTime) {
            currentState.update(gameTime);

            currentState = currentState.nextState();

            base.Update(gameTime);
        }

        /// This is called when the game should draw itself.
        protected override void Draw(GameTime gameTime) {
            currentState.draw(gameTime);

            base.Draw(gameTime);
        }
    }
}
