﻿using KineScan.BachelorProject;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {
    /**
     * The state of the scanner where we visualize the plain color image, and wait for a click in the green part of the platform
     */
    class FindRotationAxisState : ScannerState {

        // The radius of the Lego green axis thingy
        private const float LEGO_AXIS_RADIUS = 0.005f;

        // The color image bitmap
        private Texture2D colorVideo;
        private Vector3 axis;
        private bool done;
        private bool clicked;

        public FindRotationAxisState(KinectSensor kinect, GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, SpriteFont font)
        : base(kinect, graphicsDevice, spriteBatch, font) {

            colorVideo = new Texture2D(graphicsDevice, kinect.DepthStream.FrameWidth, kinect.DepthStream.FrameHeight);
            done = false;
            clicked = false;

            Console.WriteLine("Now in FindRotationAxisState - Please click on the rotation axis");
        }

        public override void update(GameTime gameTime) {
            // Get the color frame, convert from BGR into RGB, and save it into the texture
            using (ColorImageFrame colorFrame = kinect.ColorStream.OpenNextFrame(0)) {
                if (colorFrame != null) {
                    byte[] colorData = colorFrame.GetRawPixelData();
                    for (int i = 0; i < colorData.Length; i += colorFrame.BytesPerPixel) {
                        byte tmp = colorData[i];
                        colorData[i] = colorData[i + 2];
                        colorData[i + 2] = tmp;
                    }
                    colorVideo = new Texture2D(graphicsDevice, colorFrame.Width, colorFrame.Height);
                    colorVideo.SetData(colorData);

                    // Check if the mouse has been pressed
                    MouseState currentMouse = Mouse.GetState();
                    if (!clicked
                        && currentMouse.LeftButton == ButtonState.Pressed
                        && currentMouse.X > 0
                        && currentMouse.Y > 0
                        && currentMouse.X < colorFrame.Width
                        && currentMouse.Y < colorFrame.Height) {
                        using (DepthImageFrame depthFrame = kinect.DepthStream.OpenNextFrame(0)) {
                            DepthImagePixel[] depthData = depthFrame.GetRawPixelData();
                            SkeletonPoint[] skeletonPoints = new SkeletonPoint[colorFrame.Width * colorFrame.Height];
                            kinect.CoordinateMapper.MapColorFrameToSkeletonFrame(Scanner.COLOR_FORMAT, Scanner.DEPTH_FORMAT, depthData, skeletonPoints);
                            SkeletonPoint axisSkeletonpoint = skeletonPoints[currentMouse.Y * colorFrame.Width + currentMouse.X];
                            axis = new Vector3(-axisSkeletonpoint.X, axisSkeletonpoint.Y, axisSkeletonpoint.Z + LEGO_AXIS_RADIUS); // Note that it is -X, Y, Z because the X axis is inverted.
                            clicked = true;
                        }
                    }

                    // Check if mouse has then been released
                    if (clicked && currentMouse.LeftButton == ButtonState.Released) {
                        done = true;
                    }
                }
            }
        }

        public override ScannerState nextState() {
            if (!done) {
                return this;
            } else {
                return new FindObjectState(kinect, graphicsDevice, spriteBatch, font, axis);
            }
        }

        public override void draw(GameTime gameTime) {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend);
            spriteBatch.Draw(colorVideo, new Rectangle(0, 0, colorVideo.Width, colorVideo.Height), Color.White);
            spriteBatch.DrawString(font, "Please click on the rotation axis", Vector2.One, Color.Black);
            spriteBatch.DrawString(font, "Please click on the rotation axis", Vector2.Zero, Color.White);
            spriteBatch.End();
        }
    }
}
