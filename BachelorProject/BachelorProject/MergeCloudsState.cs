﻿using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KineScan {
    /*
     * state of the scanner where we take all clouds as input and output a single one
     */
    class MergeCloudsState : ScannerState {

        private List<PointCloud> clouds;
        private BasicEffect effect;
        private ViewHandler viewHandler;
        
        public MergeCloudsState(KinectSensor kinect, 
            GraphicsDevice graphicsDevice, 
            SpriteBatch spriteBatch, 
            SpriteFont font,
            List<PointCloud> clouds, 
            BasicEffect effect,
            ViewHandler viewHandler)

        : base(kinect, graphicsDevice, spriteBatch, font) {

            this.clouds = clouds;
            this.effect = effect;
            this.viewHandler = viewHandler;

            Console.WriteLine("Now in MergeCloudsState - Please wait while clouds are being merged");
        }

        public override void update(GameTime gameTime) {
            effect.View = viewHandler.updateViewport();
        }

        public override void draw(GameTime gameTime) {
            graphicsDevice.Clear(Color.White);

            foreach (EffectPass effectPass in effect.CurrentTechnique.Passes) {
                effectPass.Apply();

                foreach (PointCloud cloud in clouds) {
                    if (cloud.Length > 0) {
                        graphicsDevice.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, cloud.getDrawableList(), 0, cloud.Length, VertexPositionColor.VertexDeclaration);
                    }
                }
            }
        }

        public override ScannerState nextState() {
            PointCloud finalCloud = PointCloudFactory.emptyPointCloud();
            foreach (PointCloud cloud in clouds) {
                finalCloud.add(cloud);
            }
            return new BuildTriangleMeshState(kinect, graphicsDevice, spriteBatch, font, finalCloud, effect, viewHandler);
        }
    }
}
