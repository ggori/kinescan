﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace KineScan {

    /*
     * A vertex of the cloud.
     * Contains information about the position and the color
     */
    public class CloudVertex {
        public Vector3 vector;
        public Color color;

        public CloudVertex(Vector3 vector, Color color) {
            this.vector = vector;
            this.color = color;
        }

        public CloudVertex(float x, float y, float z, int r, int g, int b) {
            this.vector = new Vector3(x, y, z);
            this.color = new Color(r, g, b);
        }
    }

    /*
     * A point cloud
     */
    public class PointCloud : IEnumerable<CloudVertex> {
        private List<CloudVertex> cloud;
        private int count;

        // Create an empty point cloud
        public PointCloud() {
            cloud = new List<CloudVertex>();
            count = 0;
        }

        // Create an empty point cloud with a predefined size
        public PointCloud(int size) {
            cloud = new List<CloudVertex>(size);
            count = 0;
        }

        // Create an empty point cloud with a predefined width and height
        public PointCloud(int width, int height) {
            cloud = new List<CloudVertex>(width * height);
            count = 0;
        }

        // Add a vertex to the point cloud
        public void add(CloudVertex v) {
            cloud.Add(v);
            ++count;
        }

        // Add another point cloud to this point cloud
        public void add(PointCloud otherCloud) {
            foreach (CloudVertex v in otherCloud) {
                cloud.Add(v);
                ++count;
            }
        }

        // Get the size (number of vertices) of the point cloud
        public int Length {
            get { return count; }
        }

        // Needed in order to iterate over the point cloud using a foreach loop
        public IEnumerator<CloudVertex> GetEnumerator() {
            foreach (CloudVertex v in cloud) {
                yield return v;
            }
        }

        // Needed in order to iterate over the point cloud using a foreach loop
        IEnumerator IEnumerable.GetEnumerator() {
            return this.GetEnumerator();
        }

        // Translate the point cloud by the given vector
        public void translate(Vector3 v) {
            foreach (CloudVertex p in cloud) {
                p.vector = p.vector + v;
            }
        }

        // Rotate the point cloud by the given angle (in radians) around the UP vector (0, 1, 0)
        public void rotate(float angle) {
            Quaternion rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, angle);
            foreach (CloudVertex p in cloud) {
                p.vector = Vector3.Transform(p.vector, rotation);
            }
        }

        // Draw the given point cloud
        public VertexPositionColor[] getDrawableList() {
            const float size = 0.0005f; // length of the small line of each point
            Vector3 offset = new Vector3(size, size, size);
            int i = 0;
            VertexPositionColor[] vertices = new VertexPositionColor[this.Length * 2]; // 2 because we are drawing a small line for each point, and a line has 2 vertices.
            foreach (CloudVertex v in cloud) {
                vertices[i].Color = v.color;
                vertices[i].Position = v.vector;
                ++i;
                vertices[i].Color = v.color;
                vertices[i].Position = v.vector + offset;
                ++i;
            }

            return vertices;
        }
    }
}
